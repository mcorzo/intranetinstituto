﻿
namespace Intranet.Common.Enums
{
    public enum EnumAdminMenu
    {
        DatosPersonales = 1,
        Cursos = 2,
        RegistroAlumnos = 3,
        RegistroProfesores = 4,
        Notas = 5
    }
}