﻿
namespace Intranet.Common.Enums
{
    public enum EnumInstructorMenu
    {
        DatosPersonales = 1,
        Cursos = 2,
        Notas = 3,
        Matricula = 4
    }
}