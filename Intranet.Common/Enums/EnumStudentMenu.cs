﻿
namespace Intranet.Common.Enums
{
    public enum EnumStudentMenu
    {
        DatosPersonales = 1,
        Cursos = 2,
        Notas = 3,
        Matricula = 4
    }
}