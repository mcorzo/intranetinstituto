﻿using Intranet.Common.Enums;
using Intranet.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Intranet.Common.ViewModels
{
    public class EnrollmentListViewModel
    {
        public EnrollmentListViewModel()
        {

        }
        public EnrollmentListViewModel(Curso curso)
        {
            this.Id = curso.Id;
            this.CourseName = curso.Nombre;
            this.Department = curso.Especialidad.Descripcion;
            this.StartDate = curso.FechaInicio.ToShortDateString();
            this.Duration = curso.DuracionHoras;
        }
        public int Id { get; set; }

        [DisplayName("Curso")]
        public string CourseName { get; set; }

        [DisplayName("Especialidad")]
        public string Department { get; set; }

        [DisplayName("Fecha Inicio")]
        public string StartDate { get; set; }

        [DisplayName("Duración")]
        public int Duration { get; set; }
    }

    public class EnrollmentDetailViewModel
    {
        public EnrollmentDetailViewModel(Curso curso)
        {
            this.Id = curso.Id;
            this.CourseName = curso.Nombre;
            this.Department = curso.Especialidad.Descripcion;
            this.StartDate = curso.FechaInicio.ToShortDateString();
            this.Duration = curso.DuracionHoras;
            this.Description = curso.Descripcion;
            this.Vacancies = curso.Vacantes;
        }
        public int Id { get; set; }

        [DisplayName("Nombre Curso")]
        public string CourseName { get; set; }

        [DisplayName("Especialidad")]
        public string Department { get; set; }

        [DisplayName("Fecha Inicio")]
        public string StartDate { get; set; }

        [DisplayName("Duración")]
        public int Duration { get; set; }

        [DisplayName("Cantidad de Vacantes")]
        public int Vacancies { get; set; }

        [DisplayName("Descripción")]
        public string Description { get; set; }
    }

    public class EnrollmentViewModel
    {
        public EnrollmentViewModel()
        {

        }
        public string CourseName { get; set; }
        [DisplayName("Fecha Matrícula")]
        public DateTime EnrollDate { get; set; }
        [DisplayName("Número de Cuotas")]
        public int dueNumbers { get; set; }
        public int PaymentTypeId { get; set; }
        public int StudentId { get; set; }
        public int Courseid { get; set; }
    }
}
