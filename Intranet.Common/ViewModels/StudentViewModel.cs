﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Intranet.Common.ViewModels
{
    public class StudentDetailsViewModel
    {
        public StudentDetailsViewModel()
        {

        }
        public StudentDetailsViewModel(Dominio.Estudiante estudiante)
        {
            Nombres = estudiante.Nombres;
            ApellidoPaterno = estudiante.ApellidoPaterno;
            ApellidoMaterno = estudiante.ApellidoMaterno;
            NumeroCelular = estudiante.Celular;
            NumeroFijo = estudiante.Telefono;
            Email = estudiante.Email;
        }

        [DisplayName("Nombres")]
        public string Nombres { get; set; }

        [DisplayName("Apellido Paterno")]
        public string ApellidoPaterno { get; set; }

        [DisplayName("Apellido Materno")]
        public string ApellidoMaterno { get; set; }

        [Phone]
        [DisplayName("Número Celular")]
        public string NumeroCelular { get; set; }

        [Phone]
        [DisplayName("Número Telefónico")]
        public string NumeroFijo { get; set; }

        [EmailAddress]
        public string Email { get; set; }
    }
    public class StudentCourseViewModel
    {
        public StudentCourseViewModel()
        {
            SearchDepartment = Enumerable.Empty<SelectListItem>().ToList();
            CourseDetails = Enumerable.Empty<CourseViewModel>().ToList();
            CourseName = String.Empty;
        }

        public string userName { get; set; }
        [DisplayName("Especialidad")]
        public IList<SelectListItem> SearchDepartment { get; set; }
        public int idSearchDepartment { get; set; }

        [DisplayName("Curso")]
        public string CourseName { get; set; }

        public List<CourseViewModel> CourseDetails { get; set; }
    }
    public class CourseViewModel
    {
        public CourseViewModel(Dominio.Matricula matricula)
        {
            this.CourseId = matricula.CursoId;
            this.CourseName = matricula.Curso.Nombre;
            this.StartDate = matricula.Curso.FechaInicio.ToShortDateString();
            this.Department = matricula.Curso.Especialidad.Descripcion;
        }

        [DisplayName("Curso")]
        public string CourseName { get; set; }

        [DisplayName("Fecha de Inicio")]
        public string StartDate { get; set; }

        [DisplayName("Especialidad")]
        public string Department { get; set; }

        public int CourseId { get; set; }
    }
    public class RecordDetailsViewModel
    {
        public RecordDetailsViewModel()
        {

        }
    }

    public class ScoreDetailsViewModel
    {
        public ScoreDetailsViewModel(Dominio.Nota nota)
        {
            this.courseName = nota.Curso.Descripcion;
            this.nota1 = nota.nota1;
            this.nota2 = nota.nota2;
            this.nota3 = nota.nota3;
            this.evaluacionFinal = nota.evaluacionFinal;
        }

        public string courseName { get; set; }

        [DisplayName("Nota 1")]
        public int nota1 { get; set; }

        [DisplayName("Nota 2")]
        public int nota2 { get; set; }

        [DisplayName("Nota 3")]
        public int nota3 { get; set; }

        [DisplayName("Evaluación Final")]
        public int evaluacionFinal { get; set; }

        [DisplayName("Promedio Final")]
        public int promedio { get; set; }
    }
}