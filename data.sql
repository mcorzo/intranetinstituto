﻿create database [IntranetDB]
go

USE [IntranetDB]
GO
CREATE TABLE [dbo].[TipoPago] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Descripcion] VARCHAR (50) NULL,
    CONSTRAINT [PK_dbo.TipoPago] PRIMARY KEY CLUSTERED ([Id] ASC)
);
go
CREATE TABLE [dbo].[Especialidad] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Descripcion] VARCHAR (50) NULL,
    CONSTRAINT [PK_dbo.Especialidad] PRIMARY KEY CLUSTERED ([Id] ASC)
);
go
CREATE TABLE [dbo].[Profesor] (
    [Id]              INT          IDENTITY (1, 1) NOT NULL,
    [Nombres]         VARCHAR (60) NOT NULL,
    [ApellidoPaterno] VARCHAR (60) NOT NULL,
    [ApellidoMaterno] VARCHAR (60) NULL,
    [Edad]            INT          NULL,
    [Email]           VARCHAR (50) NOT NULL,
    [Celular]         VARCHAR (50) NULL,
    [Telefono]        VARCHAR (50) NULL,
    CONSTRAINT [PK_dbo.Profesor] PRIMARY KEY CLUSTERED ([Id] ASC)
);
go
CREATE TABLE [dbo].[Curso] (
    [Id]             INT             IDENTITY (1, 1) NOT NULL,
    [Nombre]         VARCHAR (100)   NOT NULL,
    [DuracionHoras]  INT             NOT NULL,
    [FechaInicio]    DATE            NOT NULL,
    [Precio]         NUMERIC (10, 2) NULL,
    [Vacantes]       INT             NOT NULL,
    [EspecialidadId] INT             NOT NULL,
    [Descripcion]    NVARCHAR (MAX)  NULL,
	[ProfesorId]	 INT			 NOT NULL,
    CONSTRAINT [PK_dbo.Curso] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Curso_dbo.Especialidad_EspecialidadId] FOREIGN KEY ([EspecialidadId]) REFERENCES [dbo].[Especialidad] ([Id]),
	CONSTRAINT [FK_dbo.Curso_dbo.Profesor_ProfesorId] FOREIGN KEY ([ProfesorId]) REFERENCES [dbo].[Profesor] ([Id])
);
go
CREATE NONCLUSTERED INDEX [IX_EspecialidadId]
    ON [dbo].[Curso]([EspecialidadId] ASC);
go
CREATE TABLE [dbo].[Estudiante] (
    [Id]              INT          IDENTITY (1, 1) NOT NULL,
    [Nombres]         VARCHAR (60) NOT NULL,
    [ApellidoPaterno] VARCHAR (60) NOT NULL,
    [ApellidoMaterno] VARCHAR (60) NULL,
    [Edad]            INT          NULL,
    [Email]           VARCHAR (50) NOT NULL,
    [Celular]         VARCHAR (50) NULL,
    [Telefono]        VARCHAR (50) NULL,
    CONSTRAINT [PK_dbo.Estudiante] PRIMARY KEY CLUSTERED ([Id] ASC)
);
go
CREATE TABLE [dbo].[Matricula] (
    [Id]             INT  IDENTITY (1, 1) NOT NULL,
    [Fecha]          DATE NOT NULL,
    [CantidadCuotas] INT  NOT NULL,
    [TipoPagoId]     INT  NOT NULL,
    [EstudianteId]   INT  NOT NULL,
    [CursoId]        INT  NOT NULL,
    CONSTRAINT [PK_dbo.Matricula] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Matricula_dbo.Curso_CursoId] FOREIGN KEY ([CursoId]) REFERENCES [dbo].[Curso] ([Id]),
    CONSTRAINT [FK_dbo.Matricula_dbo.Estudiante_EstudianteId] FOREIGN KEY ([EstudianteId]) REFERENCES [dbo].[Estudiante] ([Id]),
    CONSTRAINT [FK_dbo.Matricula_dbo.TipoPago_TipoPagoId] FOREIGN KEY ([TipoPagoId]) REFERENCES [dbo].[TipoPago] ([Id])
);
GO
CREATE NONCLUSTERED INDEX [IX_TipoPagoId]
    ON [dbo].[Matricula]([TipoPagoId] ASC);
GO
CREATE NONCLUSTERED INDEX [IX_EstudianteId]
    ON [dbo].[Matricula]([EstudianteId] ASC);
GO
CREATE NONCLUSTERED INDEX [IX_CursoId]
    ON [dbo].[Matricula]([CursoId] ASC);
GO
CREATE TABLE [dbo].[Nota] (
    [Id]              INT IDENTITY (1, 1) NOT NULL,
    [EstudianteId]    INT NOT NULL,
    [CursoId]         INT NOT NULL,
    [nota1]           INT NOT NULL,
    [nota2]           INT NOT NULL,
    [nota3]           INT NOT NULL,
    [evaluacionFinal] INT NOT NULL,
    [promedioFinal]   INT NOT NULL,
    CONSTRAINT [PK_dbo.Nota] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Nota_dbo.Curso_CursoId] FOREIGN KEY ([CursoId]) REFERENCES [dbo].[Curso] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.Nota_dbo.Estudiante_EstudianteId] FOREIGN KEY ([EstudianteId]) REFERENCES [dbo].[Estudiante] ([Id]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX [IX_EstudianteId]
    ON [dbo].[Nota]([EstudianteId] ASC);
GO
CREATE NONCLUSTERED INDEX [IX_CursoId]
    ON [dbo].[Nota]([CursoId] ASC);
go
SET IDENTITY_INSERT [dbo].[TipoPago] ON
INSERT INTO [dbo].[TipoPago] ([Id], [Descripcion]) VALUES (1, N'Pago al Contado')
INSERT INTO [dbo].[TipoPago] ([Id], [Descripcion]) VALUES (2, N'Pago con Tarjeta')
SET IDENTITY_INSERT [dbo].[TipoPago] OFF
go
SET IDENTITY_INSERT [dbo].[Especialidad] ON
INSERT INTO [dbo].[Especialidad] ([Id], [Descripcion]) VALUES (1, N'Desarrollo de Software')
INSERT INTO [dbo].[Especialidad] ([Id], [Descripcion]) VALUES (2, N'Ingeniería de Software')
INSERT INTO [dbo].[Especialidad] ([Id], [Descripcion]) VALUES (3, N'Base de Datos')
INSERT INTO [dbo].[Especialidad] ([Id], [Descripcion]) VALUES (4, N'Plataforma Tecnológica')
INSERT INTO [dbo].[Especialidad] ([Id], [Descripcion]) VALUES (5, N'Business Intelligence')
INSERT INTO [dbo].[Especialidad] ([Id], [Descripcion]) VALUES (6, N'Gestión de Proyectos')
SET IDENTITY_INSERT [dbo].[Especialidad] OFF
go
SET IDENTITY_INSERT [dbo].[Profesor] ON
INSERT INTO [dbo].[Profesor] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno], [Edad], [Email], [Celular], [Telefono]) VALUES (1, N'Ismael', N'Quinteros', N'Soto', 45, N'iquinteros@gmail.com', N'87628731', NULL)
INSERT INTO [dbo].[Profesor] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno], [Edad], [Email], [Celular], [Telefono]) VALUES (2, N'Samuel', N'Winchester', NULL, 30, N'sam@outlook.com', N'34852348', NULL)
INSERT INTO [dbo].[Profesor] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno], [Edad], [Email], [Celular], [Telefono]) VALUES (3, N'David', N'Campoblanco', NULL, 27, N'daelin@gmail.com', N'287323874', NULL)
SET IDENTITY_INSERT [dbo].[Profesor] OFF
go
SET IDENTITY_INSERT [dbo].[Curso] ON
INSERT INTO [dbo].[Curso] ([Id], [Nombre], [DuracionHoras], [FechaInicio], [Precio], [Vacantes], [EspecialidadId], [Descripcion],[ProfesorId]) VALUES (1, N'Curso XYZ', 20, N'2015-05-18', CAST(3200.00 AS Decimal(10, 2)), 25, 1, N'Curso XYZ',1)
INSERT INTO [dbo].[Curso] ([Id], [Nombre], [DuracionHoras], [FechaInicio], [Precio], [Vacantes], [EspecialidadId], [Descripcion],[ProfesorId]) VALUES (2, N'Curso XYZZ', 35, N'2015-05-18', CAST(3200.00 AS Decimal(10, 2)), 40, 2, N'Curso XYZZ',3)
SET IDENTITY_INSERT [dbo].[Curso] OFF
go
SET IDENTITY_INSERT [dbo].[Estudiante] ON
INSERT INTO [dbo].[Estudiante] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno], [Edad], [Email], [Celular], [Telefono]) VALUES (1, N'Miguel Angel', N'Corzo', N'Lequerica', 26, N'mcorzo@outlook.com', N'987815309', N'4662045')
INSERT INTO [dbo].[Estudiante] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno], [Edad], [Email], [Celular], [Telefono]) VALUES (2, N'Alfonso', N'Corzo', N'Lequerica', 19, N'pancho@gmail.com', NULL, N'4662045')
INSERT INTO [dbo].[Estudiante] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno], [Edad], [Email], [Celular], [Telefono]) VALUES (3, N'Mauricio', N'Corzo', N'Lequerica', 17, N'enano@outlook.com', N'823876823', NULL)
INSERT INTO [dbo].[Estudiante] ([Id], [Nombres], [ApellidoPaterno], [ApellidoMaterno], [Edad], [Email], [Celular], [Telefono]) VALUES (4, N'Javier', N'Corzo', N'Lequerica', 28, N'jedu786@gmail.com', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Estudiante] OFF
go