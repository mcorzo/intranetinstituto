﻿using Intranet.Common.ViewModels;
using Intranet.Dominio;
using Intranet.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Intranet.Services.Entities
{
    public class StudentService : IDisposable
    {
        private IRepository<Estudiante> _studentRepository;
        private IRepository<Matricula> _enrollRepository;
        private IRepository<Especialidad> _departmentRepository;

        public StudentService(IntranetContext context,
                                IRepository<Estudiante> repository,
                                IRepository<Matricula> enrollRepository,
                                IRepository<Especialidad> departmentRepository)
        {
            _studentRepository = repository;
            _enrollRepository = enrollRepository;
            _departmentRepository = departmentRepository;
        }
        public StudentDetailsViewModel getStudentDetails(string name)
        {
            var student = _studentRepository.Get().ToList().FirstOrDefault(s => s.Email == name);
            return new StudentDetailsViewModel(student);
        }
        public void updateStudentInfo(StudentDetailsViewModel model)
        {
            var student = _studentRepository.Get().ToList().FirstOrDefault(s => s.Email == model.Email); ;
            student.Email = model.Email;
            student.Nombres = model.Nombres;
            student.ApellidoPaterno = model.ApellidoPaterno;
            student.ApellidoMaterno = model.ApellidoMaterno;
            student.Celular = model.NumeroCelular;
            student.Telefono = model.NumeroFijo;

            _studentRepository.Update(student);
        }
        public StudentCourseViewModel getStudentCourses(StudentCourseViewModel model)
        {
            var courses = new StudentCourseViewModel();
            var cursos = _enrollRepository.Get().ToList()
                                .Where(m =>
                                        (m.Estudiante.Email == model.userName) && (m.Curso.Descripcion.Contains(model.CourseName)) && ((model.idSearchDepartment == 0) || m.Curso.EspecialidadId == model.idSearchDepartment)
                                       )
                                .ToList();

            if (model.idSearchDepartment != 0)
            {

            }

            courses.userName = model.userName;
            courses.idSearchDepartment = model.idSearchDepartment;

            _departmentRepository.Get().ToList().ForEach(d => courses.SearchDepartment.Add(new SelectListItem { Text = d.Descripcion, Value = d.Id.ToString() }));

            cursos.ForEach(c => courses.CourseDetails.Add(new CourseViewModel(c)));

            return courses;
        }

        public ScoreDetailsViewModel getScoreDetails(int courseId, string user)
        {
            var enroll = _enrollRepository.Get().FirstOrDefault(e => e.CursoId == courseId && e.Estudiante.Email == user);
            var scoreDetails = enroll.Estudiante.Nota.FirstOrDefault(n => n.CursoId == courseId && n.Estudiante.Email == user);

            return new ScoreDetailsViewModel(scoreDetails);
        }
        public void Dispose()
        {
            _studentRepository = null;
        }
    }
}
