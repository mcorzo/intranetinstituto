﻿using Intranet.Common.ViewModels;
using Intranet.Dominio;
using Intranet.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Intranet.Services.Entities
{
    public class EnrollmentService : IDisposable
    {
        private IRepository<Curso> _courseRepository;

        public EnrollmentService(IntranetContext context, IRepository<Curso> repository)
        {
            _courseRepository = repository;
        }

        public IEnumerable<EnrollmentListViewModel> getAvailableCourses()
        {
            List<EnrollmentListViewModel> cursos = new List<EnrollmentListViewModel>();
            _courseRepository.Get().ToList().ForEach(x => cursos.Add(new EnrollmentListViewModel(x)));
            return cursos;
        }

        public EnrollmentDetailViewModel getCoursesDetail(int id)
        {
            var curso = _courseRepository.Get().ToList().FirstOrDefault(c => c.Id == id);
            return new EnrollmentDetailViewModel(curso);
        }

        public void Dispose()
        {
            _courseRepository = null;
        }
    }
}
