namespace Intranet.Dominio
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inasistencia")]
    public partial class Inasistencia : EntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        [Column(TypeName = "smalldatetime")]
        public DateTime FechaInasistencia { get; set; }

        public int CursoId { get; set; }

        public int EstudianteId { get; set; }

        public bool? Justificacion { get; set; }

        public virtual Curso Curso { get; set; }

        public virtual Curso Curso1 { get; set; }
    }
}
