namespace Intranet.Dominio
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Matricula")]
    public partial class Matricula : EntityBase
    {
        [Column(TypeName = "date")]
        public DateTime Fecha { get; set; }

        public int CantidadCuotas { get; set; }

        public int TipoPagoId { get; set; }

        public int EstudianteId { get; set; }

        public int CursoId { get; set; }

        public virtual Curso Curso { get; set; }

        public virtual Estudiante Estudiante { get; set; }

        public virtual TipoPago TipoPago { get; set; }
    }
}
