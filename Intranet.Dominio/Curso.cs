namespace Intranet.Dominio
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Curso")]
    public partial class Curso : EntityBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Curso()
        {
            Inasistencia = new HashSet<Inasistencia>();
            Inasistencia1 = new HashSet<Inasistencia>();
            Matricula = new HashSet<Matricula>();
            Nota = new HashSet<Nota>();
        }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        public int DuracionHoras { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaInicio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Precio { get; set; }

        public int Vacantes { get; set; }

        public int EspecialidadId { get; set; }

        public string Descripcion { get; set; }

        public int ProfesorId { get; set; }

        public virtual Especialidad Especialidad { get; set; }

        public virtual Profesor Profesor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inasistencia> Inasistencia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inasistencia> Inasistencia1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Matricula> Matricula { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota> Nota { get; set; }
    }
}
