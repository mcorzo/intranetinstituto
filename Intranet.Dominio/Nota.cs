namespace Intranet.Dominio
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Nota")]
    public partial class Nota:EntityBase
    {
        public int EstudianteId { get; set; }

        public int CursoId { get; set; }

        public int nota1 { get; set; }

        public int nota2 { get; set; }

        public int nota3 { get; set; }

        public int evaluacionFinal { get; set; }

        public int promedioFinal { get; set; }

        public virtual Curso Curso { get; set; }

        public virtual Estudiante Estudiante { get; set; }
    }
}
