﻿using Intranet.Common.Enums;
using Intranet.Common.ViewModels;
using Intranet.Services.Entities;
using System;
using System.Web.Mvc;

namespace Intranet.Web.Controllers
{
    [Authorize(Roles = "Student")]
    public class StudentHomeController : Controller
    {
        StudentService _studentService;
        public StudentHomeController(StudentService studentService)
        {
            _studentService = studentService;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home";
            return View();
        }

        public ActionResult Details()
        {
            ViewBag.Title = "Datos Personales";
            ViewBag.SelectedMenu = EnumStudentMenu.DatosPersonales;
            var student = _studentService.getStudentDetails(User.Identity.Name);
            return View(student);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(StudentDetailsViewModel model)
        {
            ViewBag.Title = "Datos Personales";
            ViewBag.SelectedMenu = EnumStudentMenu.DatosPersonales;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Email = User.Identity.Name;
                    _studentService.updateStudentInfo(model);
                    return View(model);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex);
                    return View(model);
                }
            }
            ModelState.AddModelError("Error", "Model is invalid");
            return View(model);
        }

    }
}