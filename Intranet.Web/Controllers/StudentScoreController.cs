﻿using Intranet.Common.Enums;
using System.Web.Mvc;

namespace Intranet.Web.Controllers
{
    [Authorize(Roles = "Student")]
    public class StudentScoreController : Controller
    {
        // GET: StudentScore
        public ActionResult Index()
        {
            ViewBag.Title = "Notas Actuales";
            ViewBag.SelectedMenu = EnumStudentMenu.Notas;
            return View();
        }
    }
}