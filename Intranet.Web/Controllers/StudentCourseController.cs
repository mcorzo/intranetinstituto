﻿using Intranet.Common.Enums;
using Intranet.Common.ViewModels;
using Intranet.Services.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Intranet.Web.Controllers
{
    [Authorize(Roles = "Student")]
    public class StudentCourseController : Controller
    {
        private StudentService _studentservice;
        public StudentCourseController(StudentService studentservice)
        {
            _studentservice = studentservice;
        }
        // GET: StudentCourse
        public ActionResult Index(StudentCourseViewModel model)
        {
            ViewBag.Title = "Cursos";
            ViewBag.SelectedMenu = EnumStudentMenu.Cursos;

            model.userName = User.Identity.Name;
            model.idSearchDepartment = model.idSearchDepartment;
            model.CourseName = (string.IsNullOrEmpty(model.CourseName)) ? "" : model.CourseName;

            var cursos = _studentservice.getStudentCourses(model);
            return View(cursos);
        }

        public PartialViewResult GetScoreDetails(int courseId)
        {
            var score = _studentservice.getScoreDetails(courseId,User.Identity.Name);
            return PartialView("_PartialScoreDetails", score);
        }

        public PartialViewResult GetRecord(int courseId)
        {

            return PartialView("Shared/_PartialRecordDetails", null);
        }
    }
}