﻿using Intranet.Common.Enums;
using Intranet.Common.ViewModels;
using Intranet.Services.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Intranet.Web.Controllers
{
    [Authorize(Roles = "Student")]
    public class StudentEnrollmentController : Controller
    {
        private EnrollmentService _enrollmentservice;
        public StudentEnrollmentController(EnrollmentService enrollmentService)
        {
            _enrollmentservice = enrollmentService;
        }
        // GET: StudentEnrollment
        public ActionResult Index()
        {

            ViewBag.Title = "Matricula";
            ViewBag.SelectedMenu = EnumStudentMenu.Matricula;
            var courses = _enrollmentservice.getAvailableCourses();
            return View(courses);
        }

        public ActionResult Details(int id)
        {
            ViewBag.Title = "Detalle de Curso";
            ViewBag.SelectedMenu = EnumStudentMenu.Matricula;
            var detailCourse = _enrollmentservice.getCoursesDetail(id);
            return View(detailCourse);
        }

        public ActionResult Enrollment(int id)
        {
            ViewBag.Title = "Matricula OnLine";
            ViewBag.SelectedMenu = EnumStudentMenu.Matricula;
            var detailCourse = _enrollmentservice.getCoursesDetail(id);
            var enroll = new EnrollmentViewModel();
            enroll.Courseid = detailCourse.Id;
            enroll.CourseName = detailCourse.CourseName;

            IEnumerable<EnumPaymentType> paymentTypes = Enum.GetValues(typeof(EnumPaymentType))
                                                       .Cast<EnumPaymentType>();

            //enroll.PaymentTypeList = from types in paymentTypes
            //                         select new SelectListItem
            //                         {
            //                             Text = types.ToString(),
            //                             Value = ((int)types).ToString()
            //                         };
            return View(enroll);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Enrollment(EnrollmentViewModel model)
        {
            //ViewBag.Title = "Matricula OnLine";
            //ViewBag.SelectedMenu = EnumStudentMenu.Matricula;
            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        model.Email = User.Identity.Name;
            //        _studentService.updateStudentInfo(model);
            //        return View(model);
            //    }
            //    catch (Exception ex)
            //    {
            //        ModelState.AddModelError("Error", ex);
            //        return View(model);
            //    }
            //}
            //ModelState.AddModelError("Error", "Model is invalid");
            return View(model);
        }
    }
}