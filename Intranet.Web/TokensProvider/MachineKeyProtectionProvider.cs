﻿using System.Web.Security;
using Microsoft.Owin.Security.DataProtection;

namespace Intranet.TokensProvider
{
    public class MachineKeyProtectionProvider : IDataProtectionProvider
    {

        public IDataProtector Create(params string[] purposes)
        {
            return new MachinekeyDataProtector(purposes);
        }
    }

    public class MachinekeyDataProtector : IDataProtector
    {
        private readonly string[] _purposes;

        public MachinekeyDataProtector(string[] purposes)
        {
            _purposes = purposes;
        }
        public byte[] Protect(byte[] userData)
        {
            return MachineKey.Protect(userData, _purposes);
        }

        public byte[] Unprotect(byte[] protectedData)
        {
            return MachineKey.Unprotect(protectedData, _purposes);
        }
    }
}
