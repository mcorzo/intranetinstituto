﻿using System;
using System.Linq;
using Intranet.Dominio;

namespace Intranet.Repository
{
    public interface IRepository<TEntity> : IDisposable
        where TEntity : EntityBase
    {
        IQueryable<TEntity> Get();
        void Create(TEntity entity);
        void Update(TEntity entity);
    }
}