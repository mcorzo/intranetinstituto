using System.Data.Entity;
using Intranet.Dominio;

namespace Intranet.Repository
{
    public partial class IntranetContext : DbContext
    {
        public IntranetContext()
            : base("name=DataConnection")
        {
        }

        public virtual DbSet<Curso> Curso { get; set; }
        public virtual DbSet<Especialidad> Especialidad { get; set; }
        public virtual DbSet<Estudiante> Estudiante { get; set; }
        public virtual DbSet<Matricula> Matricula { get; set; }
        public virtual DbSet<Profesor> Profesor { get; set; }
        public virtual DbSet<TipoPago> TipoPago { get; set; }
        public virtual DbSet<Nota> Nota { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Curso>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Curso>()
                .Property(e => e.Precio)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Curso>()
                .HasMany(e => e.Matricula)
                .WithRequired(e => e.Curso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Especialidad>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Especialidad>()
                .HasMany(e => e.Curso)
                .WithRequired(e => e.Especialidad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.Nombres)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.ApellidoPaterno)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.ApellidoMaterno)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.Celular)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .HasMany(e => e.Matricula)
                .WithRequired(e => e.Estudiante)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profesor>()
                .Property(e => e.Nombres)
                .IsUnicode(false);

            modelBuilder.Entity<Profesor>()
                .Property(e => e.ApellidoPaterno)
                .IsUnicode(false);

            modelBuilder.Entity<Profesor>()
                .Property(e => e.ApellidoMaterno)
                .IsUnicode(false);

            modelBuilder.Entity<Profesor>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Profesor>()
                .Property(e => e.Celular)
                .IsUnicode(false);

            modelBuilder.Entity<Profesor>()
                .Property(e => e.Telefono)
                .IsUnicode(false);

            modelBuilder.Entity<TipoPago>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoPago>()
                .HasMany(e => e.Matricula)
                .WithRequired(e => e.TipoPago)
                .WillCascadeOnDelete(false);
        }
    }
}
