﻿using Intranet.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intranet.Repository
{
    public class DepartmentRepository : BaseRepository<Especialidad, IntranetContext>
    {
        public DepartmentRepository(IntranetContext context) : base(context)
        {
        }
    }
}
