﻿using Intranet.Dominio;

namespace Intranet.Repository
{
    public class EnrollRepository : BaseRepository<Matricula, IntranetContext>
    {
        public EnrollRepository(IntranetContext context) : base(context)
        {

        }
    }
}
