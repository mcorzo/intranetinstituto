﻿using Intranet.Dominio;

namespace Intranet.Repository
{
    public class StudentRepository : BaseRepository<Estudiante, IntranetContext>
    {
        public StudentRepository(IntranetContext context) 
            : base(context)
        {

        }
    }
}
