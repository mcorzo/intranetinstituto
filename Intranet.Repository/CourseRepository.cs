﻿using Intranet.Dominio;

namespace Intranet.Repository
{
    public class CourseRepository : BaseRepository<Curso, IntranetContext>
    {
        public CourseRepository(IntranetContext context) : base(context)
        {

        }
    }
}
